package com.example.eval.controller;

import static org.hamcrest.CoreMatchers.is;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.controller.FoodController;
import com.example.model.Food;
import com.example.service.FoodService;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class FoodControllerUnitTest {
	
	@Mock
	private FoodService fServ;
	
	@InjectMocks
	private FoodController fCon;
	
	private Food food;
	
	private MockMvc mock;

	@BeforeEach
	public void setUp() throws Exception {
		food =new Food(3, "Pizza", 1500);
		mock = MockMvcBuilders.standaloneSetup(fCon).build();
		doNothing().when(fServ).insertFood(food);
		doNothing().when(fServ).deleteFood(food);
		when(fServ.getFoodByName("Pizza")).thenReturn(food);
	}
	
	@Test
	public void postFoodTest() throws Exception {
		mock.perform(post("/foods/validate").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(food)))
			.andExpect(status().isCreated()).andExpect(jsonPath("$").value("Resource Created"));
	}
	
	
	@Test
	public void deleteFoodTest() throws Exception {
		mock.perform(delete("/foods/{foodName}", food.getFoodName()))
			.andExpect(status().isGone()).andExpect(jsonPath("$").value("Resouce Deleted"));
	}
	
	@Test
	public void getFoodTest() throws Exception {
		mock.perform(get("/foods/{foodName}", food.getFoodName()))
			.andExpect(status().isOk()).andExpect(jsonPath("$.foodName", is(food.getFoodName())))
			.andExpect(jsonPath("$.calories", is(food.getCalories())));
	}
	
	@Test
	public void putNotAllowedTest() throws Exception{
		mock.perform(put("/foods/validate")).andDo(print()).andExpect(status().isMethodNotAllowed());
	}
	
	
	
}
