package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Food;

/*
 * Spring data will provide implementation to our dao interface that extends a repository interface
 * 
 * CrudRepositorty --Extends--> PagingAndSortingRepository --Extends--> JpaRepository
 * 
 * 
 * CrudRepository provides basic CRUD functions, will make this interface have .save(), .findOne() which acts
 * 	like the get method from hibernate and find a single record by id, findAll(), delete(), .count() pre-implemented.
 * 
 * PagingAndSortingRepository provides methods to do pagination and sort records, all of the above functions, but also can
 * 	create Pageable object from the query and has properties of the construnctor of page size, current page number, sorting
 * 
 * JpaRepostitory provide JPA related methods such as flushing context cashe, batch delete records and many more, all the 
 * 	above methods and flush(), saveandFlush(), deleteInBatch()
 * 
 *so all of these repositories will need method signitures for complex queries, but springdata will provide implementation
 *	based on the method name 
 *
 *Documentation on method naming conventions:
 *
 *https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
 */

public interface FoodDao extends JpaRepository<Food, Integer> { // the 
	//Repository needs the object it is working with, and the type of the primary key as the arguments to generics
	
	public List<Food> findAll();
	public Food findByFoodName(String foodName);
	public List<Food> findByCalories(int cal);
	public Food findByFoodNameAndCalories(String foodName, int cal);

}
